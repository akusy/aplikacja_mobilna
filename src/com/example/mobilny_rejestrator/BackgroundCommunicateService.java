package com.example.mobilny_rejestrator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BackgroundCommunicateService extends Service {

	public int RECORD_TIME = 1000 * 5;
	public String auth_token;

	private boolean loopStarted = false;
	private Handler handler = new Handler();

	public boolean started = false;

	public int startRecord() {
		Intent intent = new Intent(getBaseContext(),
				BackgroundVideoRecorder.class);
		intent.putExtra("auth_token", auth_token);
		startService(intent);
		started = true;
		return 0;
	}

	public int stopRecord() {
		stopService(new Intent(getBaseContext(), BackgroundVideoRecorder.class));

		started = false;
		return 0;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	private Runnable runnable = new Runnable() {
		@Override
		public void run() {
			new AsyncHttpPostMessage("myserver").execute(auth_token);
			if (loopStarted) {
				startLoop();
			}
		}
	};

	public void stopLoop() {
		loopStarted = false;
		handler.removeCallbacks(runnable);
	}

	public void startLoop() {
		loopStarted = true;
		handler.postDelayed(runnable, RECORD_TIME);
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_ALARM, true);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(AudioManager.STREAM_DTMF,
				true);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_MUSIC, true);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(AudioManager.STREAM_RING,
				true);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_SYSTEM, true);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_VOICE_CALL, true);
		Toast.makeText(this, "Usługa uruchomiona", Toast.LENGTH_LONG).show();
		auth_token = intent.getStringExtra("auth_token");
		startLoop();

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_ALARM, false);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(AudioManager.STREAM_DTMF,
				false);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_MUSIC, false);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(AudioManager.STREAM_RING,
				false);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_SYSTEM, false);
		((AudioManager) getApplicationContext().getSystemService(
				Context.AUDIO_SERVICE)).setStreamMute(
				AudioManager.STREAM_VOICE_CALL, false);

		Toast.makeText(this, "Usługa zatrzymana", Toast.LENGTH_LONG).show();
		stopLoop();
		stopRecord();
		Log.i("BackgroundCommunicateService", "Usługa zatrzymana");
	}

	// ==========================ASYNC
	// COMUNICATION=========================================
	public class AsyncHttpPostMessage extends AsyncTask<String, Void, String> {

		private String order;

		public AsyncHttpPostMessage(final String server) {
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://192.168.1.244:3000/order");
				MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
						.create();
				entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				entityBuilder.addTextBody("auth_token", params[0]);

				HttpEntity entity = entityBuilder.build();

				post.setEntity(entity);

				HttpResponse response = client.execute(post);

				HttpEntity httpEntity = response.getEntity();

				String result = EntityUtils.toString(httpEntity);
				JSONObject jsonObj = new JSONObject(result);
				order = jsonObj.get("order").toString();
				Log.v("result", order);
				Log.v("status", response.getStatusLine().toString());

				if (order.equals("record_start")) {
					if (!started) {
						startRecord();
					} else {
						stopRecord();
						startRecord();
					}
				} else if (order.equals("record_stop")) {
					stopRecord();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	// ==========================STOP ASYNC
	// COMMUNICATION===================================
}
