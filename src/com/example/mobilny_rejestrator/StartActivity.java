package com.example.mobilny_rejestrator;

import com.example.mobilny_rejestrator.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivity extends Activity {

	public String auth_token;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);

		Intent intent = getIntent();
		auth_token = intent.getStringExtra(MainActivity.TOKEN);

		Button btnStart = (Button) findViewById(R.id.button1);
		btnStart.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(),
						BackgroundCommunicateService.class);
				intent.putExtra("auth_token", auth_token);
				startService(intent);
			}
		});

		Button btnStop = (Button) findViewById(R.id.button2);
		btnStop.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				stopService(new Intent(getBaseContext(),
						BackgroundCommunicateService.class));
			}
		});

	}

}
