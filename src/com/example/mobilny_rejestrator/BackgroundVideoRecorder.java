package com.example.mobilny_rejestrator;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.graphics.PixelFormat;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

public class BackgroundVideoRecorder extends Service implements
		SurfaceHolder.Callback {

	private static final String TAG = "null";
	public String filename;
	public String auth_token;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	private WindowManager windowManager;
	private SurfaceView surfaceView;
	private android.hardware.Camera camera = null;
	private MediaRecorder mediaRecorder = null;

	@SuppressLint("NewApi")
	public int onStartCommand(Intent intent, int flags, int startId) {
		auth_token = intent.getStringExtra("auth_token");
		Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();

		// Create new SurfaceView, set its size to 1x1, move it to the top left
		// corner and set this service as a callback
		windowManager = (WindowManager) this
				.getSystemService(Context.WINDOW_SERVICE);
		surfaceView = new SurfaceView(this);
		android.view.WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
				1, 1, WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
				WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
				PixelFormat.TRANSLUCENT);
		layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
		windowManager.addView(surfaceView, layoutParams);
		surfaceView.getHolder().addCallback(this);
		return START_STICKY;

	}

	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {

		try {

			camera = Camera.open(0);
			mediaRecorder = new MediaRecorder();
			camera.unlock();

			mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
			mediaRecorder.setCamera(camera);
			mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
			mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

			filename = Environment.getExternalStorageDirectory() + "/"
					+ System.currentTimeMillis() + ".mp4";
			mediaRecorder.setOutputFile(filename);
		} catch (Exception e) {
			Log.d(TAG, "Error setting camera preview: " + e.getMessage());
		}
		try {
			mediaRecorder.setVideoSize(640, 480);
			mediaRecorder.prepare();
			mediaRecorder.start();
		} catch (Exception e) {
		}

	}

	// Stop recording and remove SurfaceView
	@Override
	public void onDestroy() {
		Toast.makeText(this, "Service stoped", Toast.LENGTH_LONG).show();
		mediaRecorder.stop();
		mediaRecorder.reset();
		mediaRecorder.release();

		camera.lock();
		camera.release();

		windowManager.removeView(surfaceView);

		new AsyncHttpPostFile(auth_token).execute(new File(filename));
	}

	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int format,
			int width, int height) {
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
	}

	public class AsyncHttpPostFile extends AsyncTask<File, Void, String> {

		public String server;

		public AsyncHttpPostFile(final String server) {
			// this.server = server;
		}

		@Override
		protected String doInBackground(File... params) {
			try {
				HttpClient client = new DefaultHttpClient();

				HttpPost post = new HttpPost("http://192.168.1.244:3000/video");

				MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
						.create();
				entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				entityBuilder.addTextBody("auth_token", auth_token);

				if (params[0] != null) {
					entityBuilder.addBinaryBody("file", params[0]);
				}

				HttpEntity entity = entityBuilder.build();

				post.setEntity(entity);

				HttpResponse response = client.execute(post);

				HttpEntity httpEntity = response.getEntity();

				String result = EntityUtils.toString(httpEntity);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

}