package com.example.mobilny_rejestrator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.example.mobilny_rejestrator.R;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	public static String TOKEN;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final EditText email = (EditText) findViewById(R.id.editText1);
		final EditText password = (EditText) findViewById(R.id.editText2);
		Button btnLogin = (Button) findViewById(R.id.button2);
		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				new AsyncHttpPostLogin("order").execute(email.getText()
						.toString(), password.getText().toString());
			}

		});
	}

	public void runStart(String auth_token) {
		Intent intent = new Intent(this, StartActivity.class);
		intent.putExtra(TOKEN, auth_token);
		startActivity(intent);
	}

	// ==========================ASYNC
	// COMUNICATION=========================================
	public class AsyncHttpPostLogin extends AsyncTask<String, Void, String> {

		private String message;

		public AsyncHttpPostLogin(final String server) {
		}

		@Override
		protected String doInBackground(String... params) {

			try {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://192.168.1.244:3000/login");
				MultipartEntityBuilder entityBuilder = MultipartEntityBuilder
						.create();
				entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

				entityBuilder.addTextBody("email", params[0]);
				entityBuilder.addTextBody("password", params[1]);

				HttpEntity entity = entityBuilder.build();

				post.setEntity(entity);

				HttpResponse response = client.execute(post);

				HttpEntity httpEntity = response.getEntity();

				String result = EntityUtils.toString(httpEntity);
				JSONObject jsonObj = new JSONObject(result);
				message = jsonObj.get("auth_token").toString();

				String status = response.getStatusLine().toString();

				if (status.equals("HTTP/1.1 200 OK") && message.length() > 16) {
					runStart(message);
				} else {

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	// ==========================STOP ASYNC
	// COMMUNICATION===================================

}
